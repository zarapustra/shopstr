Shopstr::Application.routes.draw do

  devise_for :users, :controllers => {:registrations => 'registrations'}
  devise_scope :users do
    get "/login" => "devise/sessions#new"
    get "/logout" => "devise/sessions#destroy"
  end
  resources :authentications, :only => [:destroy, :create, :index]
  resources :carts, :only => [:update, :destroy]
  resources :items, :only => :show
  resources :line_items, :only => [:create, :destroy]
  resources :orders, :only => [:new, :show,  :create]
  resources :pages , :only => [:home, :search, :show]
  resources :sections, :only => :show do
    get :filter, :on => :member
  end
  resources :users, :only => :show


  namespace :admin do
    resources :sections do
      get :trigger, :on => :member
      get :autocomplete, :on => :member
      resources :items
      resources :parameters
      collection { post :sort }

    end
    resources :amounts
    resources :line_items
    resources :carts do
      get :add_item, :on => :member
    end
    resources :users, :path => "/users" do
      get :trigger, :on => :member
    end
    resources :orders do
      get :add_item, :on => :member
      get :cancel, :on => :member
      get :complete, :on => :member
    end
    resources :items do
      get :who_bought, :on => :member
      get :duplicate, :on => :member
      get :trigger, :on => :member
      collection { get :parameters_list }
      collection { post :sort }
      collection { post :massacre }
    end
    resources :images
    resources :parameters do
      get :trigger, :on => :member
      collection { post :sort }
      resources :amounts
      collection { get :items_list }
    end
    resources :settings do
      collection do
        put 'update'
      end
    end
    resources :groups do
      resources :users
    end
    resources :pages do
      get :trigger, :on => :member
    end
  end

  get '/auth/:provider/callback' => 'authentications#create'
  #Page.enabled.each do |page|
  #  get "/#{page.uri}" => "pages#show", :id => page.id
  #end
  get '/search', :to => 'pages#search'
  get '/admin', :to => 'admin/pages#home'
  root :to => 'pages#home'
  get '*a', :to => 'errors#routing'
end

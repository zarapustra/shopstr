Shopstr::Application.configure do
  # Settings specified here will take precedence over those in config/application.rb

  # In the development environment your application's code is reloaded on
  # every request.  This slows down response time but is perfect for development
  # since you don't have to restart the web server when you make code changes.
  config.cache_classes = false

  config.eager_load = false
  config.serve_static_assets = false
  # Show full error reports and disable caching
  config.consider_all_requests_local = true
  config.action_controller.perform_caching = false

  # Don't care if the mailer can't send
  config.action_mailer.raise_delivery_errors = false

  # Print deprecation notices to the Rails logger
  config.active_support.deprecation = :log

  # Expands the lines which load the assets
  config.assets.debug = false

  config.assets.compile = true

  #devise default_url
  config.action_mailer.default_url_options = {:host => 'localhost:3000'}

  #notifier options
  config.action_mailer.delivery_method = :smtp
  config.action_mailer.smtp_settings = {
      :address => "smtp.gmail.com",
      :port => 587,
      :domain => "Mysite.org",
      :authentication => 'plain',
      :user_name => "problema2000@gmail.com",
      :password => "5125gal9163",
      :enable_starttls_auto => true
  }

  #imagemagick
  Paperclip.options[:command_path] = "C:/ImageMagick-6.7.3-Q16"

end

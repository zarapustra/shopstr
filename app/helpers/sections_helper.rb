# -*- encoding : utf-8 -*-
module SectionsHelper
  #определятор активной ссылки
  def section_link(text, section, enabled_items, html_options={})
    active_section_id=params[:id]
    if section.id.to_s==active_section_id
      #current link
      raw("<li class='active'>"<<link_to(text, section, html_options)<<"</li>")
    else
      if enabled_items>0
        raw("<li>"<<link_to(text, section, html_options)<<"</li>")
      else
        raw("<li>"<<content_tag(:a, text, html_options)<<"<li>")
      end
    end
  end
end





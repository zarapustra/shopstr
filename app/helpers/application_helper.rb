# -*- encoding : utf-8 -*-
module ApplicationHelper

  # Return a title on a per-page basis.
  def title
    base_title = ""
    if @title.nil?
      base_title
    else
      "#{@title}"
    end
  end

  def meta_title
    if @meta_title.nil?
      ""
    else
      "#{@meta_title}"
    end
  end

  def meta_description
    if @meta_description.nil?
      "Язычник: магазин учебников"
    else
      "#{@meta_description}"
    end
  end

  def meta_keywords
    if @meta_keywords.nil?
      "Язычник,магазин,учебники, иностранные, языки, пособия"
    else
      "#{@meta_keywords}"
    end
  end

  def hidden_div_if(condition, attributes = {}, &block)
    if condition
      attributes["style"] = "display: none"
    end
    content_tag("div", attributes, &block)
  end

  def sortable(column, title = nil)
    css_class = column == sort_column ? "current #{sort_direction}" : nil
    direction = column == sort_column && sort_direction == "asc" ? "desc" : "asc"
    link_to title, params.merge(:sort => column, :direction => direction, :page => nil), {:class => css_class, remote: true}
  end

  def per_page(per,title= nil)
    title||=per
    css_class = cookies.signed[:per_page]==per.to_s || params[:per_page]==per.to_s  ? "current per_page" : "per_page"
    link_to title, params.merge(:per_page => per, :page=>nil), {:class => css_class, remote: true}
  end

  #devise для использования форм в любом месте 3 функции
  def resource_name
    :user
  end

  def resource
    @resource ||= User.new
  end

  def devise_mapping
    @devise_mapping ||= Devise.mappings[:user]
  end


  def redirect_back_or(default)
    redirect_to(session[:return_to] || default)
    clear_return_to
  end

  def store_location
    session[:return_to] = request.fullpath
  end

  private

  def clear_return_to
    session[:return_to] = nil
  end

end

module OrdersHelper
  def orders_chart_series(orders, start_time)
    orders_by_day = orders.where(:updated_at => start_time.beginning_of_day..Time.zone.now.end_of_day)
    .group("date(updated_at)")
    .select("date(updated_at) as updated_at, sum(totals) as totals")
    (start_time.to_date..Date.today).map do |date|
      order = orders_by_day.detect { |order| order.updated_at.to_date == date }
      order && order.totals.to_i || 0
    end
  end
def orders_chart_month_series(orders,start_time)
    orders_by_month = orders.where(:updated_at => start_time.at_midnight..Time.zone.now.end_of_day)
    .select("date_trunc('month', updated_at) as month, sum(totals) as totals").group("month")

    (start_time.month..Date.today.month).map do |month|
      order = orders_by_month.detect { |order| order.month.to_date.month == month }
      order && order.totals.to_i || 0
    end
  end
def orders_numbers_chart_series(orders,start_time)
    orders_by_month = orders.where(:updated_at => start_time.at_midnight..Time.zone.now.end_of_day)
    .select("date_trunc('month', updated_at) as month, COUNT(id) as counter").group("month")

    (start_time.month..Date.today.month).map do |month|
      order = orders_by_month.detect { |order| order.month.to_date.month == month }
      order && order.counter.to_i || 0
    end
  end
end
# -*- encoding : utf-8 -*-
module ItemsHelper
  def filter_link(text, filters={}, html_options={})
    trigger=0
    params_to_keep = [:section, :enabled, :group, :status]
    params_to_keep.each do |param|
      if filters[param].to_s==params[param] && filters[param].to_s!="clear" || filters[param].to_s=="clear"&&params[param].nil?
        trigger=1
      end
      if filters[param]=="clear"
        filters.delete(param)
      else
        filters[param]=params[param] if filters[param].nil?
      end
    end
    html_options[:class]= 'current' if trigger==1
    link_to text, filters, html_options
  end

  #формирует фильтры для категорий.
  def param_filter_link(text, section, filters={}, html_options={})
    trigger=0
    params_to_keep = section.parameters.select(:title).map { |obj| obj.title }
    params_to_keep.each do |param|
      if filters[param].to_s==params[param] && filters[param].to_s!="clear" || filters[param].to_s=="clear"&&params[param].nil?
        #добавляется, чтобы появилась кнопка 'закрыть'
        filters[param]="clear"
        trigger=1
      end
      if filters[param]=="clear"
        filters.delete(param)
      else
        filters[param]=params[param] if filters[param].nil?
      end
    end
    if trigger==1
      #current link
      sanitize("<span class=current>#{text}"<<link_to("X", filters, :title => "Сбросить фильтр")<<"</span>")
    else
      sanitize("<span>"<<link_to(text, filters, html_options)<<"</span>")
    end
  end

  def item_par_amount(item, param)
    (item.amounts&param.amounts).pop
  end

  def filter_link_li(text, filters={}, html_options={})
    trigger=0
    params_to_keep = [:section, :enabled, :group, :status]
    params_to_keep.each do |param|
      if filters[param].to_s==params[param] && filters[param].to_s!="clear" || filters[param].to_s=="clear"&&params[param].nil?
        trigger=1
      end
      if filters[param]=="clear"
        filters.delete(param)
      else
        filters[param]=params[param] if filters[param].nil?
      end
    end
    if trigger==1
      #current link
      raw("<li class=active>"<<link_to(text, filters, html_options)<<"</li>")
    else
      raw("<li>"<<link_to(text, filters, html_options)<<"</li>")
    end
  end
end
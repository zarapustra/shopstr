# -*- encoding : utf-8 -*-
module UsersHelper

  def admin_user
    redirect_to(root_path) unless current_user.try(:admin?)
  end

  def correct_user?(id)
    user = User.find_by_id(id)
    unless current_user.nil?
      current_user==user
    else
      false
    end
  end

end

class Image < ActiveRecord::Base
  belongs_to :item
  belongs_to :page
  default_scope { order('created_at DESC') }
  has_attached_file :photo, :styles => {:medium => "150x200", :thumb => "45x>"}
  before_post_process :rename_photo

  def rename_photo
    #avatar_file_name - important is the first word - avatar - depends on your column in DB table
    extension = File.extname(photo_file_name).downcase
    self.photo.instance_write :file_name, "#{Time.now.to_i.to_s}#{extension}"
  end
end
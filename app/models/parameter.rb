# -*- encoding : utf-8 -*-
class Parameter < ActiveRecord::Base
  has_and_belongs_to_many :sections
  has_many :amounts, :dependent => :destroy

  #acts_as_list
  default_scope { order('parameters.position ASC') }

end

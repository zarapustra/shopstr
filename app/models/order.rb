# -*- encoding : utf-8 -*-
class Order < ActiveRecord::Base
  before_save :add_hash, :count_total
  has_many :line_items, :dependent => :destroy
  belongs_to :user
  accepts_nested_attributes_for :line_items
  default_scope { order('orders.created_at DESC').joins("LEFT JOIN line_items li ON li.order_id = orders.id").select("orders.*, sum(li.quantity) as quantity").group("orders.id") }
  scope :newest, where(:status => 0)
  scope :checked, where(:status => 1)
  scope :completed, where(:status => 2)
  scope :deleted, where(:status => 3)

  PAYMENT_TYPES = [["Оплата наличными курьеру", 0]]
  STATUS= [["Новый", 0], ["Принят", 1], ["Выполнен", 2], ["Удален", 3]]

  validates :name, :address, :phone, :pay_type, :presence => true
  validates :pay_type, :inclusion => PAYMENT_TYPES.map { |key, value| value }
  #validates :phone, :length => 1..10

  def add_line_items_from_cart(cart)
    cart.line_items.each do |item|
      item.cart_id = nil
      line_items << item
    end
  end

  #def total_items
  #  line_items.sum(:quantity)
  #end

  def self.get(field, value)
    where(field => value)
  end

  def self.in_group(group)
    threshold=group.threshold
    next_threshold=Group.where("threshold > ? AND wholesale='t'", threshold).first.try(:threshold)
    unless next_threshold.blank?
      orders=Order.where(:totals => (threshold..next_threshold)).order('created_at DESC')
    else
      orders=Order.where("totals>=?", threshold).order('created_at DESC')
    end
    orders
  end

  def total
    line_items.sum("price*quantity").to_i
  end

  private

  def add_hash
    self.email_hash=Digest::SHA2.hexdigest("#{self.email}--#{Time.now.utc}")
  end

  def count_total
    self.totals=self.total unless self.new_record?
  end

end

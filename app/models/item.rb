# -*- encoding : utf-8 -*-
class Item < ActiveRecord::Base

  before_destroy :destroy_items_in_carts

  validates :title, :presence => true,
            :length => {:maximum => 100},
            :uniqueness => true
  validates :price, :numericality => true

  belongs_to :section
  has_many :line_items
  has_many :images, :dependent => :destroy
  has_one :main_image, :conditions => "main = 't'", :class_name => 'Image'
  has_many :minor_images, :class_name => 'Image', :conditions => "main = 'f'"

  has_and_belongs_to_many :amounts
  accepts_nested_attributes_for :images, :reject_if => lambda { |t| t['image'].nil? }

  def self.enabled
    where(:enabled => true)
  end

  def self.recommended
    where(:enabled => true, :recommend => true)
  end

  def self.get(field, value)
    where(field => value)
  end

  private
  def destroy_items_in_carts
    line_items.each do |li|
      li.destroy
    end
    true
  end


end





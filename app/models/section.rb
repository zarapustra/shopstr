# -*- encoding : utf-8 -*-
class Section < ActiveRecord::Base
  #attr_accessible :title, :parent_id, :level, :description, :keywords
  attr_protected
  validates :title, :presence => true,
            :length => {:maximum => 50},
            :uniqueness => true
  validates :parent_id, :numericality => true

  has_many :items, :dependent => :destroy
  has_and_belongs_to_many :parameters
  has_many :amounts, :through => :items, :dependent => :destroy
  #has_and_belongs_to_many :amounts
  TYPES= [["Строка", 0], ["Число", 1], ["Логика", 2]]

  #счетчик активных товаров и вложенные категории
  default_scope { joins("LEFT JOIN items items1 ON items1.section_id = sections.id AND items1.enabled='t'")
                .joins("LEFT JOIN items items2 ON items2.section_id = sections.id")
                .select("sections.*, count(distinct items1.id) as enabled_items, count(distinct items2.id) as all_items")
                .group("sections.id")
                .includes(:children)
                }
  has_many :children, :class_name => 'Section', :foreign_key => 'parent_id'
  belongs_to :parent, :class_name => 'Section'

  def self.tree(full=false)
    beginner=Section.first

    if beginner
      tree=[]<<beginner
      result_tree=structurizer(tree,beginner.children)
      if full
        result_tree
      else
        result_tree.delete(beginner)
        if result_tree.size>0
          result_tree
        end
      end
    end
  end

  private
  def self.structurizer(tree,parents)
    for parent in parents
      tree<<parent
      structurizer(tree,parent.children)
    end
    tree
  end
end

class Page < ActiveRecord::Base
  has_many :images, :dependent => :destroy
  validates :title, :presence => true, :uniqueness => true
  validates :uri, :presence => true, :uniqueness => true
  before_save :uri_cleaning

  def uri_cleaning
    self.uri=self.uri.downcase.strip.gsub(/\s+/, "_")
  end

  def self.enabled
    where(:enabled=>true)
  end
end
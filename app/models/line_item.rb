# -*- encoding : utf-8 -*-
class LineItem < ActiveRecord::Base
  belongs_to :order
  belongs_to :item
  belongs_to :cart
  attr_protected
  default_scope { order('line_items.created_at DESC') }

  serialize :options

  validates_numericality_of :quantity, :greater_than => 0

  def total
    price*quantity
  end

end

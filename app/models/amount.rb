# -*- encoding : utf-8 -*-
class Amount < ActiveRecord::Base
  belongs_to :parameter
  has_and_belongs_to_many :items
  #validates_numericality_of :num, :only_integer => true
  KINDS= {0 => :str, 1 => :num, 2 => :logic}

  def self.get_amounts(item, parameter)
    amounts=item.amounts
    unless amounts[parameter.id.to_s].nil?
      Amount.find(amounts[parameter.id.to_s])
    else
      []
    end
  end

end

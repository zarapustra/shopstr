# -*- encoding : utf-8 -*-
class User < ActiveRecord::Base
  # Include default devise modules. Others available are:
  # :token_authenticatable, :encryptable, :lockable, :timeoutable and :omniauthable
  devise :database_authenticatable, :registerable, :confirmable,
         :recoverable, :rememberable, :trackable, :validatable, :omniauthable
  attr_accessible :email, :remember_me, :address, :password, :remember_me

  email_regex = /\A[\w+\-.]+@[a-z\d\-.]+\.[a-z]+\z/i
  validates :email, :format => {:with => email_regex},
            :uniqueness => {:case_sensitive => false}
  has_many :orders
  has_many :authentications

  def sum_of_done
    self.orders.completed.sum(:totals)
  end

  def last_order
    Order.where('user_id=?', id).order('created_at DESC').first
  end

  def self.in_group(group)
    threshold=group.threshold
    next_th=Group.where("threshold > ? AND wholesale= ?", threshold, false).select(:threshold).limit(1)
    unless next_th.empty?
      users=User.where("spent BETWEEN ? AND ?", threshold, next_th[0].threshold)
    else
      users=User.where("spent>=?", threshold)
    end
    users
  end

  def group
    groups=Group.za.where("threshold<=?", spent).limit(1)
    unless groups.empty?
      groups[0]
    else
      nil
    end
  end

  def apply_omniauth(omniauth)
    #debugger
    #self.email = omniauth['user_info']['email'] if email.blank?
    authentications.build(:provider => omniauth['provider'], :uid => omniauth['uid'])
  end

  def password_required?
    (authentications.empty? || !password.blank?) && super
  end

  def self.get(field, value)
    where(field => value)
  end
end

# -*- encoding : utf-8 -*-
class Setting < ActiveRecord::Base
  attr_protected
  validates :option, :presence => true,
            :length => {:maximum => 50},
            :uniqueness => true
  validates :value, :presence => true
  VALUE_TYPES= {0 => "целое число", 1 => "текст", 2 => "да/нет"}

end

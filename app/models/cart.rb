# -*- encoding : utf-8 -*-
class Cart < ActiveRecord::Base
  has_many :line_items, :dependent => :destroy
  has_many :items, :through => :line_items
  accepts_nested_attributes_for :line_items
  #default_scope joins("LEFT JOIN line_items li ON li.cart_id = carts.id").select("carts.*, sum(li.price*li.quantity) as total").group("carts.id")

  def quantity
    line_items.sum(:quantity)
  end

  def total
    line_items.sum("price*quantity").to_i
  end
end

class Group < ActiveRecord::Base
  attr_accessible :title, :threshold, :discount

  validates :title, :presence => true,
            :length => {:maximum => 50},
            :uniqueness => true
  validates :threshold, :presence => true,
            :numericality => true
  validates :discount, :presence => true,
            :numericality => true

  scope :az, :order => ('groups.threshold ASC')
  scope :za, :order => ('groups.threshold DESC')

  def count_users
    next_threshold=Group.az.where("threshold > ?", threshold).first.try(:threshold)
    if next_threshold.blank?
      User.where("spent>=?", threshold).count
    else
      User.where(:spent => (threshold..next_threshold)).count
    end
  end

  def count_orders
    next_threshold=Group.where("threshold > ? AND wholesale='t'", threshold).first.try(:threshold)
    if next_threshold.blank?
      Order.unscoped.where("totals>=#{threshold}").count
    else
      Order.unscoped.where(:totals => (threshold..next_threshold)).count
    end
  end

  def get_users
    next_threshold=Group.az.where("threshold > ?", threshold).first.try(:threshold)
    if next_threshold.blank?
      User.where("spent>=?", threshold)
    else
      User.where(:spent => (threshold..next_threshold))
    end
  end

end

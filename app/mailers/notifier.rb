# -*- encoding : utf-8 -*-
class Notifier < ActionMailer::Base
  default :from => 'Книжная полка <problema2000@gmail.com>'

  # Subject can be set in your I18n file at config/locales/action_view.active_model.active_record.active_support.will_paginate.custom.ru.yml
  # with the following lookup:
  #
  #   en.notifier.order_received.subject
  #
  def order_received(order)
    @order = order
    mail(:to => order.email,
         :subject => 'Подтверждение заказа в Книжной полке') do |format|
      format.html
      #format.text
    end
  end

  # Subject can be set in your I18n file at config/locales/action_view.active_model.active_record.active_support.will_paginate.custom.ru.yml
  # with the following lookup:
  #
  #   en.notifier.order_shipped.subject
  #
  def order_accepted(order)
    @order = order
    mail(:to => order.email,
         :subject => 'Заказ из Книжной полки обработан') do |format|
      format.html
      #format.text
    end
  end
end

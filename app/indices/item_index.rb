ThinkingSphinx::Index.define :item, :with => :active_record do
  indexes title, :sortable => true
  # attributes
  has price
  has created_at

end

# this procudes the <?xml ... ?> tag at the start of the document
#   note: this is different to calling builder normally as the <?xml?> tag
#         is very different to how you'd write a normal tag!
xml.instruct! :xml, :version => '1.0', :encoding => 'UTF-8'

# create the urlset
xml.yml_catalog :date => DateTime.now.stamp("%Y-%m-%d %H:%m") do
  xml.shop do
    xml.name "Язычник"
    xml.company "ИП Говнолюди"
    xml.url root_url
    xml.platform "SvoyaCMS"
    xml.version "1.0"
    xml.email "problema2000@gmail.com"
    xml.currencies do
      xml.currency :id => "RUR", :rate => "1", :plus => "0"
    end
    xml.categories do
      @sections.each do |cat|
        unless cat.parent.id==1
          xml.category cat.title, :id => cat.id, :parentId => cat.parent.id
        else
          xml.category cat.title, :id => cat.id
        end
      end
    end
    xml.local_delivery_cost @delivery_cost
    xml.offers do
      @items.each do |item|
        xml.offer :id => item.id, :type => "book", :available => item.in_stock? do
          xml.url item_url(item)
          xml.price item.price
          xml.categoryId item.section_id
          xml.picture item.image.url
          xml.store "false"
          xml.pickup "false"
          xml.delivery "true"
          xml.author
          xml.name "#{item.title}"
          xml.description item.description
          xml.publisher
          xml.series
          xml.year
          xml.ISBN
          xml.volume
          xml.part
          xml.description
          xml.downloadable "false"
          item.section.parameters.each do |par|
            am=item_par_amount(item, par)
            xml.param am.value, :type => par.title unless am.blank?
          end
        end
      end
    end
  end
end
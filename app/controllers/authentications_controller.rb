# -*- encoding : utf-8 -*-
class AuthenticationsController < ApplicationController
  def index
    @authentications = current_user.authentications if signed_in?
  end

  def create
    omniauth = request.env["omniauth.auth"]
    authentication = Authentication.find_by_provider_and_uid(omniauth['provider'], omniauth['uid'])
    if authentication
      flash[:notice] = "Вы успешно вошли."
      sign_in_and_redirect(:user, authentication.user)
    elsif current_user
      current_user.authentications.create!(:provider => omniauth['provider'], :uid => omniauth['uid'])
      flash[:notice] = "Аутентификация прошла успешно"
      redirect_to authentications_url
    else

      user = User.new
      user.apply_omniauth(omniauth)

      if user.save
        flash[:notice] = "Вы успешно вошли"
        sign_in_and_redirect(:user, user)
      else
        session[:omniauth] = omniauth.except('extra')
        redirect_to new_user_registration_url
      end
    end
  end

  def destroy
    @authentication = current_user.authentications.find(params[:id])
    @authentication.destroy
    flash[:notice] = "Successfully destroyed authentication."
    redirect_to :back
  end

  protected

# This is necessary since Rails 3.0.4
# See https://github.com/intridea/omniauth/issues/185
# and http://www.arailsdemo.com/posts/44
#  def handle_unverified_request
#    true
#  end
end

class ApplicationController < ActionController::Base
  protect_from_forgery
  before_filter :set_time_zone
  include UsersHelper
  include ApplicationHelper
  helper_method :sort_column, :sort_direction, :current_cart
  private

  def set_time_zone
    min = request.cookies["time_zone"].to_i
    Time.zone = ActiveSupport::TimeZone[min.minutes]
  end

  def current_cart
    Cart.find(cookies.signed[:cart_id])
  rescue ActiveRecord::RecordNotFound
    cart=Cart.create
    cookies.permanent.signed[:cart_id] = cart.id
    cart
  end

  def sections_tree
    @sections=Section.tree
  end

  def pages_list
    @pages=Page.enabled
  end

  def section_references_actualizer(section)
    child_level=section.level+1
    section.children.each do |child|
      child.update_attribute(:level, "#{child_level}")
      section_references_actualizer(child)
    end
  end

  def log(obj)
    logger.info(obj)
  end

  def sort_column
    Item.column_names.include?(params[:sort]) ? params[:sort] : "position"
  end

  def sort_direction
    %w[asc desc].include?(params[:direction]) ? params[:direction] : "asc"
  end

  def per_value
    if params[:per_page]
      cookies.permanent.signed[:per_page] =params[:per_page]
    else
      cookies.signed[:per_page]||=Setting.find_by_option("items_per_page").value.to_i
    end
  end

end

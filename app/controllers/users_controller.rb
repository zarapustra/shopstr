# -*- encoding : utf-8 -*-
class UsersController < ApplicationController
  before_filter :pages_list
  def show
    if correct_user?(params[:id])
      @user = User.find(params[:id])
      @title = "Ваши заказы"
      @orders=@user.orders.page(params[:page]).per(10)
      @statuses=Hash[Order::STATUS.map { |key, value| [value, key] }]
    end
  end
end

# -*- encoding : utf-8 -*-
class Admin::SettingsController < Admin::BaseController

  def index
    @title='Настройки'
    @settings = Setting.all
    @sections=Section.tree
    @value_types=Setting::VALUE_TYPES

    respond_to do |format|
      format.html # index.html.erb
      format.json { render json: @settings }
    end
  end

  def update
    params[:settings].keys.each do |s_id|
      value=params[:settings][s_id].to_i
      Setting.find(s_id.to_i).update_attribute(:value, value)
    end
    respond_to do |format|
      format.html { redirect_to admin_settings_path, notice: 'Настройки обновлены!' }
      format.json { head :ok }
    end
  end

=begin

  def new
    @title='Новая настройка'
    @setting = Setting.new

    respond_to do |format|
      format.html { render '_form' }
      format.json { render json: @setting }
    end
  end

  def edit
    @title='Изменение настройки'
    @setting = Setting.find(params[:id])
    @sections=Section.sections_tree

  end

  def create
    @setting = Setting.new(params[:setting])
    respond_to do |format|
      if @setting.save
        format.html { redirect_to admin_edit_setting_path(@setting), notice: 'Настройка создана.' }
        format.json { render json: @setting, status: :created, location: @setting }
      else
        format.html { render '_form' }
        format.json { render json: @setting.errors, status: :unprocessable_entity }
      end
    end
  end

  def destroy
    @setting = Setting.find(params[:id])
    @setting.destroy

    respond_to do |format|
      format.html { redirect_to admin_settings_url }
      format.json { head :ok }
    end
  end

=end

end

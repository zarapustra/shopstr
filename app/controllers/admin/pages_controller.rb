# -*- encoding : utf-8 -*-
class Admin::PagesController < Admin::BaseController
  before_filter :sections_tree

  def home
    @new_counter=Order.unscoped.where(:status => 0).count
  end

  def index
    @title='Все страницы'
    @pages=Page.all
  end

  def new
    @page=Page.new
    @title='Новая страница'
    @new_images=Image.where(:item_id => nil, :page_id => nil)
    render '_form'

  end

  def create
    @title='Новая страница'
    @page=Page.new(params[:page])
    @new_images=Image.where(:item_id => nil, :page_id => nil)

    if @page.save
      @page.images<<@new_images
      Devise::Application.reload_routes!
      redirect_to admin_pages_path
    else
      render '_form'
    end
  end

  def edit
    @page=Page.find(params[:id])
    @title='Изменение страницы'
    @new_images=Image.where(:item_id => nil, :page_id => nil)

    render '_form'
  end

  def update
    @title='Изменение страницы'
    @page=Page.find(params[:id])
    @new_images=Image.where(:item_id => nil, :page_id => nil)
    if @page.update_attributes((params[:page]))
      @page.images<<@new_images

      Devise::Application.reload_routes!
      redirect_to admin_pages_path
    else
      render '_form'
    end
  end
  def trigger
      @page= Page.find(params[:id])
      method= params[:field]
      value= @page.send(method) ? false : true
      if @page.update_attribute(method, value)
        respond_to do |format|
          format.html { redirect_to :back }
          format.js
        end
      end
    end
  
    def destroy
      page=Page.find(params[:id])
      page.destroy
      flash[:success] = 'Страница была успешно удалена.'
      respond_to do |format|
        format.html { redirect_to admin_pages_path }
        format.js
      end
    end
  
end

# -*- encoding : utf-8 -*-
class Admin::CartsController < Admin::BaseController
  before_filter :admin_user

  def index
    @carts = Cart.all

    respond_to do |format|
      format.html # index.html.erb
      format.json { render json: @carts }
    end
  end

  def destroy
    @cart = current_cart
    @cart.destroy
    cookies.permanent.signed[:cart_id] = nil

    respond_to do |format|
      format.html { redirect_to('/', :notice => 'Ваша корзина пуста') }
      format.js
      format.json { head :ok }
    end
  end

  def update
    @cart=Cart.find(params[:id])
    @cart.update_attributes(params[:cart])
    respond_to do |format|
      format.js
    end
  end

  def add_item
    @cart=current_cart
    item_id=params[:item_id]
    if item_id
      current_item = @cart.line_items.find_by_item_id(item_id)
      if current_item
        current_item.quantity += 1
        current_item.save
      else
        @cart.line_items.create(:item_id => item_id, :price => Item.find(item_id).price)
      end
    end
    respond_to do |format|
      format.js
    end
  end

end

# -*- encoding : utf-8 -*-
class Admin::LineItemsController < Admin::BaseController

  def create
    @cart = current_cart
    item = Item.find(params[:item_id])
    @line_item = @cart.add_item(item.id)

    respond_to do |format|
      if @line_item.save
        format.html { redirect_to('/') }
        format.js { @current_item = @line_item }
        format.xml { render :xml => @line_item,
                            :status => :created, :location => @line_item }
      else
        format.html { render :action => "new" }
        format.xml { render :xml => @line_item.errors,
                            :status => :unprocessable_entity }
      end
    end
  end

  def update
    @line_item = LineItem.find(params[:id])

    respond_to do |format|
      if @line_item.update_attributes(params[:line_item])
        format.html { redirect_to @line_item, notice: 'Line item was successfully updated.' }
        format.json { head :ok }
      else
        format.html { render action: "edit" }
        format.json { render json: @line_item.errors, status: :unprocessable_entity }
      end
    end
  end

  def destroy
    line_item = LineItem.find(params[:id])
    @order=line_item.order
    line_item.destroy

    respond_to do |format|
      format.html { redirect_to :back }
      format.js
    end
  end

end

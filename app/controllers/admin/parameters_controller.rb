# -*- encoding : utf-8 -*-
class Admin::ParametersController < Admin::BaseController
  before_filter :sections_tree, :except => [:destroy, :trigger, :sort]

  def index
    @title = "Параметры"
    @parameters = Parameter.page(params[:page]).per(10)
    if params[:section]
      @parameters=Section.find(params[:section]).parameters.page(params[:page]).per(10)
    end
    store_location
  end

  def new
    @title="Создание нового параметра"
    @parameter=Parameter.new
    respond_to do |format|
      format.html { render '_form' }
      format.js
    end
  end

  def create
    @parameter=Parameter.new(params[:parameter])
    @selected_sections= Section.find(params[:section_ids]) if params[:section_ids]
    @amounts=@parameter.amounts
    @parameter.sections=@selected_sections
    if @parameter.save
      create_amounts
      flash[:success] = "Параметр "+@parameter.title+" создан!"
      redirect_to admin_parameters_path
    else
      render '_form'
    end
  end

  def edit
    @title = "Изменение параметра"
    @parameter = Parameter.find(params[:id])
    @amounts=@parameter.amounts
    @selected_sections=@parameter.sections.includes(:items)
    render '_form'
  end

  def update
    @parameter = Parameter.find(params[:id])
    @amounts=@parameter.amounts

    if params[:section_ids].blank?
      @parameter.sections.clear
    else
      @selected_sections= Section.find(params[:section_ids])
      @parameter.sections=@selected_sections
    end
    if @parameter.update_attributes(params[:parameter])
      if params[:section_ids]
        create_amounts
      else
        @parameter.amounts.each do |am|
          am.items.clear
          am.destroy
        end
      end
      flash[:success] = "Параметр "+@parameter.title+" изменен"
      redirect_back_or admin_parameters_path
    else
      @title = "Изменение параметра"
      render '_form'
    end
  end

  def destroy
    parameter=Parameter.find(params[:id])
    tmp=parameter.title
    parameter.destroy
    flash[:success] = 'Параметр "' + tmp +'" был успешно удален.'
    redirect_to admin_parameters_path
  end

  def trigger
    @parameter= Parameter.find(params[:id])
    method= params[:field]
    value= @parameter.send(method) ? false : true
    if @parameter.update_attribute(method, value)
      respond_to do |format|
        format.html { redirect_to :back }
        format.js
      end
    end
  end

  def sort
    params[:parameter].each_with_index do |id, index|
      Parameter.update_all({position: index+1}, {id: id})
    end
    render nothing: true
  end

  def items_list
    @selected_sections= Section.find(params[:section_ids]) if params[:section_ids]
    render :layout => nil
  end

  private

  def create_amounts
    kind=Amount::KINDS[@parameter.kind]
    params[:amounts][kind].each do |item_id, new_value|
      item=Item.find(item_id)
      if params[:section_ids].include?(item.section_id.to_s)
        amounts=(item.amounts&@parameter.amounts)

        if amounts.empty?
          unless new_value.blank?
            amount=@parameter.amounts.where(kind => new_value).first
            if amount.nil?
              #создать
                amount=@parameter.amounts.create(kind => new_value)
              item.amounts<<amount
            else
              item.amounts<<amount
            end
          end
        else
          amount= amounts[0]
          if new_value.blank?
            #проверить значение на необходимость унитожения
            if amount.items.size<2
              item.amounts.delete(amount)
              amount.destroy
            end
          else
            old_value=amount.try(kind)
            unless old_value==new_value
              #обновить
              if amount.items.size>1
                item.amounts.delete(amount)
                #значение из формы есть ли в имеющихся значениях данного параметра?
                amount_exist=@parameter.amounts.where(kind => new_value).first
                if amount_exist
                  amount=amount_exist
                else
                  amount=@parameter.amounts.create(kind => new_value)
                end
                item.amounts<<amount
              else
                amount.update_attribute(kind, new_value)
              end
            end
          end
        end
      end
    end
  end
end


# -*- encoding : utf-8 -*-
class Admin::ItemsController < Admin::BaseController
  before_filter :sections_tree, :except => [:trigger, :destroy, :who_bought]
  require "benchmark"


  def index
    @order=Order.new
    @order_cart=current_cart
    @title="Товары"
    @items=Item.order('position asc')
    @items=@items.get(:enabled, params[:enabled]) if params[:enabled]
    @items=@items.get(:section_id, params[:section]) if params[:section]
    @items=@items.page(params[:page]).per(per_value)
    store_location

  end

  def show
    @item = Item.find(params[:id])
    @parameters=Parameter.all
    @section=@item.section
    @title=@item.title
  end

  def new
    @title="Создание нового товара"

    if params[:section_id]
      @section=Section.find(params[:section_id])
    else
      @section=Section.where(:level => '2').order('created_at DESC').first
    end
    @item=@section.items.new
    @parameters=@section.parameters
    @new_images=Image.where(:item_id => nil, :page_id => nil)
    render '_form'
  end

  def create
    got_images=true
    @item = Item.new(params[:item])
    @section=@item.section
    @parameters=@section.parameters
    #суета с картинками
    @new_images=Image.where(:item_id => nil, :page_id => nil)
    if params[:main_image].blank?
      if @new_images.blank?
        got_images=false
        flash[:warning]="Прикрепите хотя бы одну картинку!"
      else
        Image.update(@new_images.first.id, :main => true)
      end
    else
      Image.update(params[:main_image], :main => true)
    end
    if  got_images and @item.save
      @parameters.each do |param|
        value=params[:amounts]["#{param.id}"]
        unless value.blank?
          case param.kind
            when 0
              amount=param.amounts.find_or_create_by_str(value, :parameter_id => param.id)
            when 1
              #проверка на integer 4 byte
              if value.is_a? Integer or not value.to_i > 4
                amount=nil
                flash[:error]="Целое значение для поля '#{param.title}' не должно превышать 2147483647. Введенное вами значение НЕ СОХРАНЕНО!"
              else
                amount=param.amounts.find_or_create_by_num(value, :parameter_id => param.id)
              end
            when 2
              amount=param.amounts.find_or_create_by_logic(value, :parameter_id => param.id)
          end
          @item.amounts<<amount unless amount.nil?
        end
      end
      @item.images<<@new_images
      flash[:success] = "Новый товар <a href='#{edit_admin_item_url(@item)}'>#{@item.title}</a> сохранен!"
      redirect_to admin_items_path(:section => @item.section)
    else
      @title="Создание нового товара"
      @unsaved_amounts=params[:amounts]
      render '_form'
    end
  end

  def edit
    @title = "Изменение товара"
    @item = Item.find(params[:id])
    @section=@item.section
    @parameters=@section.parameters
    @new_images=Image.where(:item_id => nil, :page_id => nil)
    render '_form'
  end

  def duplicate
    @title="Создание нового товара из дубликата"
    @item= Item.find(params[:id]).dup
    @section=@item.section
    @parameters=@section.parameters
    @new_images=Image.where(:item_id => nil, :page_id => nil)
    render '_form'
  end


  def update
    got_images=true
    @item = Item.find(params[:id])
    old_amounts=@item.amounts.clone
    @new_images=Image.where(:item_id => nil, :page_id => nil)
    #суета с картинками
    @images=(@new_images + @item.images)
    if params[:main_image].blank?
      if @images.blank?
        got_images=false
        flash[:warning]="Прикрепите хотя бы одну картинку!"
      else
        if @item.main_image.blank?
          Image.update(@images.first, :main => true)
        end
      end
    else
      @images.each { |i| i.update_attribute(:main, false) }
      Image.update(params[:main_image], :main => true)
    end

    if got_images and @item.update_attributes(params[:item])
      @section=@item.section
      @parameters=@section.parameters
      #обновление значений
      @item.amounts.clear
      @parameters.each do |param|
        value=params[:amounts]["#{param.id}"]
        unless value.blank?
          case param.kind
            when 0
              amount=param.amounts.find_or_create_by_str(value, :parameter_id => param.id)
            when 1
              #проверка на integer 4 byte
              if value.is_a? Integer or not value.to_i > 4
                amount=nil
                flash[:error]="Целое значение для поля <b>#{param.title}</b> не должно превышать 2147483647. Введенное вами значение <b>НЕ СОХРАНЕНО!</b>"
              else
                amount=param.amounts.find_or_create_by_num(value, :parameter_id => param.id)
              end
            when 2
              amount=param.amounts.find_or_create_by_logic(value, :parameter_id => param.id)
          end
          @item.amounts<<amount unless amount.nil?
        end
      end
      @item.images<<@new_images
      #удаление неиспользуемых значений
      unused_amounts=old_amounts - @item.amounts
      unused_amounts.try(:each) do |amount|
        amount.destroy if amount.items.empty?
      end
      flash[:success] = "Товар <a href='#{edit_admin_item_url(@item)}'>#{@item.title}</a> изменен!"
      redirect_to admin_items_path(:section => @item.section)
    else
      @section=@item.section
      @parameters=@section.parameters
      @title = "Изменение товара"
      @unsaved_amounts=params[:amounts]
      render '_form'
    end
  end

  def trigger
    @item= Item.find(params[:id])
    method= params[:field]
    value= @item.send(method) ? false : true
    if @item.update_attribute(method, value)
      respond_to do |format|
        format.html { redirect_to admin_section_path(@item.section) }
        format.js
      end
    end
  end

  def destroy(id=nil)
    @item=Item.find(id||=params[:id])
    if @item.line_items.empty?
      @item.amounts.each do |amount|
        amount.destroy if amount.items.size<2
      end
      @item.amounts.clear
      @item.destroy
      @destroyed=true
      flash.now[:success] = "Товар <b>#{@item.title}</b> был успешно удален."
    else
      @destroyed=false
      @item.update_attribute(:enabled, false)
      flash.now[:warning] = "Товар <b>#{@item.title}</b> не может быть удален,так как находится в чьей-то корзине. Выключен."
    end
  end

  def who_bought
    @item = Item.find(params[:id])
    respond_to do |format|
      format.atom
      format.xml { render :xml => @item }
    end
  end

  def parameters_list
    section=Section.find(params[:section]) if params[:section]
    @parameters=section.parameters
    respond_to :js
  end

  def sort
    params[:item].each_with_index do |id, index|
      Item.update_all({position: index+1}, {id: id})
    end
    render nothing: true
  end

  def massacre
    case params[:operation]
      when '0'
        params[:items_id].each do |id|
          destroy(id)
        end
      when '1'
        unless params[:items_id].blank?
          section=Section.find(params[:section])
          Item.find(params[:items_id]).try(:each) do |item|
            #odd_amounts - значения, оставшиеся от старой категории.
            odd_amounts = item.amounts-section.amounts
            section.items<<item
            odd_amounts.each do |am|
              #параметр значения есть в новой секции
              unless section.parameters.exists?(am.parameter)
                if am.items.size>1
                  am.items.delete(item)
                else
                  am.items.destroy_all
                  am.items.clear
                end
              end
            end
          end
        end
    end
    redirect_to :back
  end
end

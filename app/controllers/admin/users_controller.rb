# -*- encoding : utf-8 -*-
class Admin::UsersController < Admin::BaseController

  def index
    @title="Клиенты"
    @users=User.page(params[:page]).per(5)
    if params[:group]
      @users=Group.find(params[:group]).get_users.page(params[:page]).per(10)
    end
    @groups=Group.all
    @sections=Section.tree

  end

  def show
    @user = User.find(params[:id])
    @title = @user.last_order.try(:name)
    @orders=@user.orders
    @sections=Section.tree
    @statuses=Hash[Order::STATUS.map { |key, value| [value, key] }]
  end

  def destroy
    user=User.find(params[:id])
    email=user.email
    user.destroy
    flash[:success] = "Пользователь #{email} был уничтожен."
    redirect_to admin_users_path
  end

  def trigger
    @user= User.find(params[:id])
    method= params[:field]
    value= @user.send(method) ? false : true
    if @user.update_attribute(method, value)
      respond_to do |format|
        format.html { redirect_to :back }
        format.js
      end
    end
  end

end

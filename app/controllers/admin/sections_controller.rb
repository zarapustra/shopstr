# -*- encoding : utf-8 -*-
class Admin::SectionsController < Admin::BaseController
  def index
    @title = "Категории"
    @sections=Section.tree
    store_location
  end

  def show
    @cart=current_cart
    @section = Section.find(params[:id])
    @items=@section.items.page(params[:page]).per(10)
    @title=@section.title
    @parameters=@section.parameters
    @sections=Section.tree
  end

  def new
    @title="Создание новой категории"
    @sections=Section.tree(true)
    @section=Section.new
    @parameters=Parameter.all
    render '_form'
  end

  def create
    @section = Section.new(params[:section])
    @section.parameters= Parameter.find(params[:parameter_ids]) if params[:parameter_ids]

    # добавляем уровень вложенности в новую секцию
    @section.level=@section.parent.level+1

    if @section.save
      flash[:success] = "Категория создана!"
      redirect_to admin_sections_path
    else
      @title="Создание новой категории"
      @sections=Section.tree(true)
      @parameters=Parameter.all
      @selected_parameters=@section.parameters
      render '_form'
    end
  end

  def edit
    @title = "Изменение категории"
    @section = Section.find(params[:id])
    @parameters=Parameter.all
    @selected_parameters=@section.parameters
    @sections=Section.tree(true)
    render '_form'
  end

  def update
    @section = Section.find(params[:id])
    @section.parameters= Parameter.find(params[:parameter_ids]) if params[:parameter_ids]

    @section.level=Section.find(params[:section][:parent_id]).level+1
    params[:section][:level]="#{@section.level}"

    if @section.update_attributes(params[:section])
      section_references_actualizer(@section)
      flash[:success] = "Категория изменена"
      redirect_back_or admin_sections_path
    else
      @title = "Изменение категории"
      render '_form'
    end
  end

  def destroy
    no_bought=false
    @id=params[:id]
    @section=Section.find(@id)
    @section.items.each do |i|
      unless i.line_items.empty?
        break
      end
      no_bought=true
    end
    title=@section.title
    if no_bought.nil?
      #section.destroy
      flash.now[:success] = "Категория <b>#{title}</b> и товары в ней были успешно удалены."
    else
      flash.now[:error] = "Категория <b>#{title}</b> не может быть удалена, т.к. хотя бы один товар в ней был куплен."
    end
    respond_to :js
  end

  def trigger
    @section= Section.find(params[:id])
    method= params[:field]
    value= @section.send(method) ? false : true
    if @section.update_attribute(method, value)
      respond_to do |format|
        format.html { redirect_to :back }
        format.js
      end
    end
  end

  def sort
    params[:section].each_with_index do |id, index|
      Section.update_all({position: index+1}, {id: id})
    end
    render nothing: true
  end

end

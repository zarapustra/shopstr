# -*- encoding : utf-8 -*-
class Admin::GroupsController < Admin::BaseController
  layout 'admin'
  before_filter :admin_user
  before_filter :sections_tree, :except => [:destroy, :update, :create]

  def index
    @title='Группы'
    @groups = Group.order('discount').all
    respond_to do |format|
      format.html
      format.json { render json: @groups }
    end
  end

  def show
    @group = Group.find(params[:id])
    @title=@group.title
    #сбор инфы в зависимости от того, оптовая группа или розничная
    if @group.wholesale?
      @orders=Order.in_group(@group).page(params[:page]).per(10)
    else
      @users=User.in_group(@group).page(params[:page]).per(10)
    end
  end

  def new
    @title='Создание группы'
    @group = Group.new
    respond_to do |format|
      format.html { render '_form' }
      format.json { render json: @group }
    end
  end

  def edit
    @title='Изменение группы'
    @group = Group.find(params[:id])
    render '_form'
  end

  def create
    @group = Group.new(params[:group])
    @title='Создание группы'
    respond_to do |format|
      if @group.save
        format.html { redirect_to admin_groups_path, notice: 'Группа была успешно создана!' }
        format.json { render json: @group, status: :created, location: @group }
      else
        format.html { render '_form' }
        format.json { render json: @group.errors, status: :unprocessable_entity }
      end
    end
  end

  def update
    @group = Group.find(params[:id])
    @title='Изменение группы'
    respond_to do |format|
      if @group.update_attributes(params[:group])
        format.html { redirect_to admin_groups_path, notice: 'Группа была успешно обновлена!' }
        format.json { head :ok }
      else
        format.html { render '_form' }
        format.json { render json: @group.errors, status: :unprocessable_entity }
      end
    end
  end

  def destroy
    @group = Group.find(params[:id])
    @group.destroy
    respond_to do |format|
      format.html { redirect_to admin_groups_url }
      format.json { head :ok }
    end
  end
  def find_group

  end
end

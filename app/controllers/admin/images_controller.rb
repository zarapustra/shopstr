# -*- encoding : utf-8 -*-
class Admin::ImagesController < Admin::BaseController

  def create
    @image=Image.create(:photo=>params[:file])
    respond_to :js
  end

  def destroy
    Image.destroy(params[:id])
    respond_to :js
  end

end


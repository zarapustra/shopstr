# -*- encoding : utf-8 -*-
class Admin::AmountsController < Admin::BaseController
  layout 'admin'
  before_filter :admin_user
  before_filter :sections_tree
  before_filter :init_amount, :except => :index

  def index
    @title = "Значения"
    @amounts = Amount.paginate(:page => params[:page])
  end

  def edit
    @title = "Изменение значения"
    render '_form'
  end

  def update
    if @amount.update_attributes(params[:amount])
      flash[:success] = "Значение изменено"
      redirect_to admin_amounts_path
    else
      @title = "Изменение значения"
      render '_form'
    end
  end

  def init_amount
    @amount = Amount.find(params[:id])
  end
end

# -*- encoding : utf-8 -*-
class Admin::OrdersController < Admin::BaseController
  before_filter :sections_tree, :except => [:destroy, :cancel, :complete, :add_item]

  def index
    @title="Заказы"
    @orders=Order.page(params[:page]).per(10)
    if params[:status]
      @orders=@orders.get(:status, params[:status])
    end
    respond_to do |format|
      format.html # index.html.erb
    end
  end

  def new
    @items=Item.enabled
    @cart=current_cart
    @order = Order.new(:status => 1)
    @title="Оформление нового заказа"
    @statuses=Hash[Order::STATUS.map { |key, value| [value, key] }]
    @delivery=Setting.find_by_option("delivery_value").value.to_i
    @threshold=Setting.find_by_option("delivery_limit").value.to_i
    @discount=0
    @total_discount=@cart.total*(100-@discount)/100
    @total_discount_delivery=@total_discount
    @total_discount_delivery+=@delivery unless @total_discount>@threshold
  end

  def edit
    @title=""
    @order=Order.find(params[:id])
    @pay_types=Hash[Order::PAYMENT_TYPES.map { |key, value| [value, key] }]
    @statuses=Hash[Order::STATUS.map { |key, value| [value, key] }]
    @delivery=Setting.find_by_option("delivery_value").value.to_i
    @threshold=Setting.find_by_option("delivery_limit").value.to_i

    user=@order.try(:user)
    @group=user.group if user

    #discount
    if @group && @order.discount==0
      @order.discount=@group.discount
    end
    @discount=@order.discount.to_i
    @total_discount=@order.total*(100-@discount)/100
    @total_discount_delivery=@total_discount
    @total_discount_delivery+=@delivery unless @total_discount>@threshold

    @items=Item.enabled

    if session[:prev]||session[:next]
      @prev=session[:prev]
      @next=session[:next]
      session[:prev]=nil
      session[:next]=nil
    else
      @prev= Order.unscoped.where("id > ? AND status = ?", params[:id], @order.status).order('created_at ASC').first
      @next= Order.unscoped.where("id < ? AND status = ?", params[:id], @order.status).order('created_at DESC').first
    end
  end

  def create
    @order = Order.new(params[:order])
    if @order.save
      @order.add_line_items_from_cart(current_cart)
      Cart.destroy(cookies.signed[:cart_id])
      cookies.permanent.signed[:cart_id] = nil

      unless @order.email.blank?
        Thread.new { Notifier.order_received(@order).deliver }
      end
      redirect_to('/', :notice => "Спасибо за ваш заказ, #{@order.name}")
    else
      @title="Оформление нового заказа"
      @items=Item.enabled
      @cart=current_cart
      @statuses=Hash[Order::STATUS.map { |key, value| [value, key] }]
      @delivery=Setting.find_by_option("delivery_value").value.to_i
      @threshold=Setting.find_by_option("delivery_limit").value.to_i
      @discount=@order.discount
      @total_discount=@cart.total*(100-@discount)/100
      @total_discount_delivery=@total_discount
      @total_discount_delivery+=@delivery unless @total_discount>@threshold
      render :new
    end
  end

  def update
    @order = Order.find(params[:id])
    user=@order.try(:user)
    @group=user.group if user
    #discount
    @discount=@order.discount
    if @group && @discount==0
      @discount=@group.discount
    end
    respond_to do |format|
      params[:order][:prev_status]=@order.status
      if @order.update_attributes(params[:order])
        case @order.status
          when 1
            Thread.new { Notifier.order_accepted(@order).deliver }
          when 2
            if user
              user.update_attribute(:spent, user.sum_of_done)
            end
        end

        session[:prev]= Order.unscoped.where("id > ? AND status = ?", params[:id], @order.prev_status).order('created_at ASC').first
        session[:next]= Order.unscoped.where("id < ? AND status = ?", params[:id], @order.prev_status).order('created_at DESC').first

        format.html { redirect_to :back, notice: 'Заказ был успешно обновлен' }
        format.js
      else
        format.html { render action: "edit", notice: 'Ошибка' }
        format.js
      end
    end
  end

  def destroy
    @order = Order.find(params[:id])
    @order.destroy

    respond_to do |format|
      format.html { redirect_to admin_orders_url }
      format.json { head :ok }
    end
  end

  def cancel
    @order = Order.find(params[:id])
    @order.update_attribute(:status, 3)
    respond_to do |format|
      format.html { redirect_to :back }
      format.js
    end
  end

  def complete
    @order = Order.find(params[:id])
    @order.update_attribute(:status, 2)
    user=@order.try(:user)
    user.update_attribute(:spent, user.sum_of_done) if user

    respond_to do |format|
      format.html { redirect_to :back }
      format.js
    end
  end

  def add_item
    @order=Order.find(params[:id])
    item_id=params[:item_id]
    if item_id
      current_item = @order.line_items.find_by_item_id(item_id)
      if current_item
        current_item.quantity += 1
        current_item.save
      else
        @order.line_items.create(:item_id => item_id, :price => Item.find(item_id).price)
      end
    end
    respond_to do |format|
      format.js
    end
  end
end


# -*- encoding : utf-8 -*-
class LineItemsController < ApplicationController

  def create
    @cart = current_cart
    @line_item = @cart.line_items.find_by_item_id(params[:item_id])
    @quantity=@cart.quantity+1

    if @line_item.blank?
      @line_item = LineItem.create(:cart_id => @cart.id, :item_id => params[:item_id], :price => Item.find(params[:item_id]).price)
    else
      @line_item.update_attribute(:quantity, @line_item.quantity+1)
      @line_item
    end
    respond_to :js
  end

  def destroy
    line_item = LineItem.find(params[:id])
    @cart=line_item.cart
    line_item.destroy

    @quantity=@cart.quantity
    respond_to do |format|
      format.html { redirect_to :back }
      format.js
    end
  end

end

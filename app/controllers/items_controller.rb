# -*- encoding : utf-8 -*-
class ItemsController < ApplicationController
  before_filter :sections_tree, :pages_list

  def show
    @item = Item.find(params[:id])
    @title=@item.title
    @section=@item.section
    @parameters=@section.parameters
    @cart=current_cart

    #meta
    @meta_title=@item.meta_title
    @meta_description=@item.meta_description
    @meta_keywords=@item.meta_keywords
  end
end

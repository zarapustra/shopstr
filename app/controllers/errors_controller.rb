# -*- encoding : utf-8 -*-
class ErrorsController < ApplicationController
  def routing
    render_404
  end

  private
  def render_404
    flash[:error]="Нет такой страницы"
    redirect_to '/'
    #render :file => "#{Rails.root}/public/404.html", :status => 404, :layout => false
  end

end


# -*- encoding : utf-8 -*-
class CartsController < ApplicationController

  def update
    @cart = Cart.find(params[:id])

    respond_to do |format|
      if @cart.update_attributes(params[:cart])
        format.html { redirect_to @cart, notice: 'Cart was successfully updated.' }
        format.js
      else
        format.html { render action: "edit" }
        format.json { render json: @cart.errors, status: :unprocessable_entity }
      end
    end
  end

  def destroy
    @cart = current_cart
    @cart.line_items.each do |li|
      li.destroy
    end

    respond_to do |format|
      format.html { redirect_to('/', :notice => 'Ваша корзина пуста') }
      format.js
    end
  end
end

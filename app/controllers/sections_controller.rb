# -*- encoding : utf-8 -*-
class SectionsController < ApplicationController
  before_filter :sections_tree, :pages_list

  def show
    @section = Section.find(params[:id])
    @title=@section.title
    @parameters=@section.parameters.where(:filterable => true).order(:kind)
    @items=@section.items.enabled.order(sort_column + " " + sort_direction).page(params[:page]).per(per_value)
    @cart=current_cart

    #meta
    @meta_title=@section.meta_title
    @meta_description=@section.meta_description
    @meta_keywords=@section.meta_keywords
  end

  def filter
    section=Section.find(params[:id])
    @items=section.items.order(sort_column + " " + sort_direction)

    #price
    from=params[:price][:from].to_i.abs
    to=params[:price][:to].to_i.abs
    unless from==0 and to==0
      if to==0
        to=1000000
      else
        to, from=from, to if from>to
      end
      @items=@items.where(:price => (from..to))
    end

    params[:amounts].each do |param_id, values|
      param=Parameter.find(param_id)
      case param.kind
        when 0
          items_str=[]
          values.each do |value|
            amount=Amount.find(value)
            items_str+=amount.items
          end
          @items&=items_str
        when 1
          from=values[:from].to_i.abs
          to=values[:to].to_i.abs
          unless from==0 and to==0
            if to==0
              to=1000000
            else
              if from>to
                to, from=from, to if from>to
              end
            end
            amounts=Amount.where(:num => (from..to))
            items_num=[]
            amounts.each do |amount|
              items_num+=amount.items
            end
            @items&=items_num
          end
        when 2
          unless values.blank?
            amount=Amount.find_by_id(values)
            @items&=amount.items
          end
      end
    end
    @items=Kaminari.paginate_array(@items).page(params[:page]).per(5)
  end

end

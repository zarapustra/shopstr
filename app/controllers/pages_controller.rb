# -*- encoding : utf-8 -*-
class PagesController < ApplicationController
  before_filter :sections_tree, :pages_list

  def home
    @meta_description = "Сайт для продажи академических изданий глупым людям"
    @meta_keywords = "Английский, учебник, литература, словарь"
    @title = "Рекомендуемые товары"
    @items = Item.recommended.order(sort_column + " " + sort_direction).page(params[:page]).per(per_value)
    @cart = current_cart

  end

  def search
    @items = Item.enabled.search(params[:search], :match_mode => :all, :order => (sort_column + " " + sort_direction)).page(params[:page]).per(2)
    @title="Найденные товары"
    @cart = current_cart
  end

  def show
    @page=Page.find(params[:id])
    @title=@page.title
    @cart = current_cart
  end

end

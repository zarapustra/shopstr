# -*- encoding : utf-8 -*-
class OrdersController < ApplicationController
  before_filter :pages_list

  def show
    @order = Order.find(params[:id])
    @statuses=Hash[Order::STATUS.map { |key, value| [value, key] }]
    @pay_types=Hash[Order::PAYMENT_TYPES.map { |key, value| [value, key] }]
    @delivery=Setting.find_by_option("delivery_value").value.to_i
    @threshold=Setting.find_by_option("delivery_limit").value.to_i
    @discount= @order.discount.to_i
    @total_discount=@order.total*(100-@discount)/100
    @total_discount_delivery=@total_discount
    @total_discount_delivery+=@delivery unless @total_discount>@threshold
    unless current_user==@order.user||params[:email_hash]==@order.email_hash
      redirect_to(root_path)
    else
      respond_to do |format|
        format.html # show.html.erb
        format.js
      end
    end
  end

  def new
    @order_cart = current_cart
    @sections = Section.tree
    @title="Оформление заказа"
    @delivery=Setting.find_by_option("delivery_value").value.to_i
    @threshold=Setting.find_by_option("delivery_limit").value.to_i
    if not signed_in? or current_user.group.blank?
      @discount=0
    else
      @discount= current_user.group.discount
    end
    @total_discount=@order_cart.total.to_i*(100-@discount)/100
    @total_discount_delivery=@total_discount
    @total_discount_delivery+=@delivery unless @total_discount>@threshold
    if @order_cart.line_items.empty?
      redirect_to '/', :notice => "Your cart is empty"
      return
    end

    if signed_in?
      @order = Order.new(:name => current_user.last_order.try(:name),
                         :email => current_user.email,
                         :address => current_user.last_order.try(:address),
                         :phone => current_user.last_order.try(:phone))
    else
      @order = Order.new
    end

    respond_to do |format|
      format.html { render '_form' }
      format.json { render json: @order }
    end
  end

  def create
    @order = Order.new(params[:order])
    @order.user=current_user
    @order.totals=current_cart.total
    if @order.save
      @order.add_line_items_from_cart(current_cart)
      Cart.destroy(cookies.signed[:cart_id])
      cookies.permanent.signed[:cart_id] = nil
      unless @order.email.blank?
        Thread.new { Notifier.order_received(@order).deliver }
      end
      redirect_to('/', :notice => "Спасибо за ваш заказ, #{@order.name}!")
    else
      render action: "new"
    end
  end

end

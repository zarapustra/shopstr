// This is a manifest file that'll be compiled into including all the files listed below.
// Add new JavaScript/Coffee code in separate files in this directory and they'll automatically
// be included in the compiled file accessible from http://example.com/assets/application.js
// It's not advisable to add code directly here, but if you do, it'll appear at the bottom of the
// the compiled file.
//
// require rails.validations
//= require jquery
// require jquery-ui
//= require jquery_ujs
//= require bootstrap.min
//= require jgrowl.min
// require fancybox.pack


//установка временной зоны в куки, используется в форме входа
function set_time_zone_offset() {
    var current_time = new Date();
    document.cookie = "time_zone=" - current_time.getTimezoneOffset();
}

$(function () {
    //модальные окна
    //$('#register').modal('hide');

    //сообщения
    function show_flashes() {
        $.each($('.flash'), function () {
            var flash = $(this);
            var msg = flash.text();
            var klass = flash.attr('class');
            if (msg != null && msg != '') {
                $.jGrowl(msg, {life:5000, theme:klass });
            }
        });
    }

    show_flashes();
    $('.navbar').on('keyup', '#search', function () {
        if ($(this).val().length > 2) {
            $(this).parent().submit();
        }
    });

    $('#left-nav a').click(function (e) {
        e.preventDefault();
        $(this).tab('show');
    });
    $('body').on('click', '.close', function () {
        $('#cartpopup,#login').popover('hide');
    });
    $('#cartpopup').click(function () {
        $('#login').popover('hide');
        $(this).popover('toggle');
    });
    $('#login').click(function () {
        $('#cartpopup').popover('hide');
        $(this).popover('toggle');
    });
    $('.shopBtn').modal('hide');
    $('.cartmodal').modal();
    $('#cartTrigger').click(function () {
        $('#cartmodal .modal-body').html($('#cart').html());

    });

    var carttrigger=$('#carttrigger');
    var quantity=carttrigger.find('span');
    $('.add-to-cart').click(function () {
        quantity.show();
        carttrigger.animate({'margin-bottom':'-=10'}, 100).animate({'margin-bottom':'+=10'}, 700, 'easeOutBounce');
        quantity.html(+quantity.html()+1);
    });

    $.jGrowl('shit', {life:5000, theme:'alert' });
});








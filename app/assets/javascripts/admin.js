// This is a manifest file that'll be compiled into including all the files listed below.
// Add new JavaScript/Coffee code in separate files in this directory and they'll automatically
// be included in the compiled file accessible from http://example.com/assets/application.js
// It's not advisable to add code directly here, but if you do, it'll appear at the bottom of the
// the compiled file.
//
// require rails.validations
//= require jquery
// require jquery-ui
//= require jquery_ujs
//= require bootstrap.min
//= require plupload.full
//= require jgrowl.min
//= require nicEdit
//= require mousewheel
//= require highcharts

//установка временной зоны в куки, используется в форме входа
function set_time_zone_offset() {
    var current_time = new Date();
    document.cookie = "time_zone=" - current_time.getTimezoneOffset();
}
function show_flashes() {
    $.each($('.flash'), function () {
        var flash = $(this);
        var msg = flash.text();
        var klass = flash.attr('class');
        if (msg != null && msg != '') {
            $.jGrowl(msg, {sticky:true, theme:klass });
        }
    });
}

$(function () {
    //сообщения
    show_flashes();
    $('#left-nav a').click(function (e) {
        e.preventDefault();
        $(this).tab('show');
    });

});








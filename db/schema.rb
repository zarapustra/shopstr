# encoding: UTF-8
# This file is auto-generated from the current state of the database. Instead
# of editing this file, please use the migrations feature of Active Record to
# incrementally modify your database, and then regenerate this schema definition.
#
# Note that this schema.rb definition is the authoritative source for your
# database schema. If you need to create the application database on another
# system, you should be using db:schema:load, not running all the migrations
# from scratch. The latter is a flawed and unsustainable approach (the more migrations
# you'll amass, the slower it'll run and the greater likelihood for issues).
#
# It's strongly recommended that you check this file into your version control system.

ActiveRecord::Schema.define(version: 20120603111000) do

  create_table "amounts", force: true do |t|
    t.integer  "parameter_id"
    t.string   "str"
    t.integer  "num"
    t.boolean  "logic"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  create_table "amounts_items", id: false, force: true do |t|
    t.integer "amount_id"
    t.integer "item_id"
  end

  create_table "authentications", force: true do |t|
    t.integer  "user_id"
    t.string   "provider"
    t.string   "uid"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  create_table "carts", force: true do |t|
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  create_table "groups", force: true do |t|
    t.string   "title"
    t.integer  "threshold"
    t.integer  "discount",   default: 0
    t.datetime "created_at"
    t.datetime "updated_at"
    t.boolean  "wholesale",  default: false
  end

  create_table "images", force: true do |t|
    t.integer  "item_id"
    t.string   "photo_file_name"
    t.string   "photo_content_type"
    t.integer  "photo_file_size"
    t.datetime "photo_updated_at"
    t.boolean  "main",               default: false
    t.datetime "created_at"
    t.datetime "updated_at"
    t.integer  "page_id"
  end

  create_table "items", force: true do |t|
    t.string   "title"
    t.text     "description"
    t.integer  "price"
    t.datetime "created_at"
    t.datetime "updated_at"
    t.integer  "section_id"
    t.boolean  "enabled",          default: true
    t.boolean  "recommend",        default: false
    t.string   "meta_keywords"
    t.boolean  "in_stock",         default: true
    t.string   "meta_title"
    t.string   "meta_description"
    t.integer  "position"
  end

  add_index "items", ["created_at"], name: "index_items_on_created_at", using: :btree
  add_index "items", ["section_id"], name: "index_items_on_section_id", using: :btree
  add_index "items", ["title"], name: "index_items_on_title", unique: true, using: :btree

  create_table "line_items", force: true do |t|
    t.integer  "item_id"
    t.integer  "cart_id"
    t.datetime "created_at"
    t.datetime "updated_at"
    t.integer  "quantity",   default: 1
    t.integer  "order_id"
    t.integer  "price"
  end

  create_table "orders", force: true do |t|
    t.string   "name"
    t.text     "address"
    t.string   "email"
    t.datetime "created_at"
    t.datetime "updated_at"
    t.integer  "user_id"
    t.integer  "discount",        default: 0
    t.string   "comment"
    t.string   "phone"
    t.string   "email_hash"
    t.integer  "status",          default: 0
    t.integer  "pay_type",        default: 0
    t.integer  "prev_status"
    t.string   "manager_comment"
    t.integer  "totals"
  end

  create_table "pages", force: true do |t|
    t.string   "title"
    t.boolean  "enabled",          default: true
    t.string   "meta_keywords"
    t.string   "meta_title"
    t.string   "meta_description"
    t.datetime "created_at"
    t.datetime "updated_at"
    t.text     "content"
    t.string   "uri"
  end

  create_table "parameters", force: true do |t|
    t.string   "title"
    t.datetime "created_at"
    t.datetime "updated_at"
    t.boolean  "filterable", default: false
    t.integer  "position"
    t.string   "measure"
    t.string   "hint"
    t.integer  "kind",       default: 0
  end

  create_table "parameters_sections", id: false, force: true do |t|
    t.integer "section_id"
    t.integer "parameter_id"
  end

  create_table "sections", force: true do |t|
    t.string   "title"
    t.integer  "parent_id",        default: 1
    t.integer  "level",            default: 1
    t.datetime "created_at"
    t.datetime "updated_at"
    t.text     "description"
    t.string   "meta_keywords"
    t.string   "meta_title"
    t.string   "meta_description"
    t.boolean  "enabled",          default: true
    t.integer  "position"
  end

  add_index "sections", ["created_at"], name: "index_sections_on_created_at", using: :btree
  add_index "sections", ["title"], name: "index_sections_on_title", unique: true, using: :btree

  create_table "settings", force: true do |t|
    t.string  "option"
    t.string  "value"
    t.string  "title"
    t.integer "value_type"
  end

  create_table "users", force: true do |t|
    t.string   "email",                              default: "",    null: false
    t.string   "encrypted_password",     limit: 128, default: "",    null: false
    t.string   "reset_password_token"
    t.datetime "reset_password_sent_at"
    t.datetime "remember_created_at"
    t.integer  "sign_in_count",                      default: 0
    t.datetime "current_sign_in_at"
    t.datetime "last_sign_in_at"
    t.string   "current_sign_in_ip"
    t.string   "last_sign_in_ip"
    t.string   "password_salt"
    t.string   "confirmation_token"
    t.datetime "confirmed_at"
    t.datetime "confirmation_sent_at"
    t.integer  "failed_attempts",                    default: 0
    t.string   "unlock_token"
    t.datetime "locked_at"
    t.string   "authentication_token"
    t.datetime "created_at"
    t.datetime "updated_at"
    t.boolean  "admin",                              default: false
    t.string   "address"
    t.integer  "spent"
    t.boolean  "enabled",                            default: true
  end

  add_index "users", ["email"], name: "index_users_on_email", unique: true, using: :btree
  add_index "users", ["reset_password_token"], name: "index_users_on_reset_password_token", unique: true, using: :btree

end

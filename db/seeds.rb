# -*- encoding : utf-8 -*-
# This file should contain all the record creation needed to seed the database with its default values.
# The data can then be loaded with the rake db:seed (or created alongside the db with db:setup).
#
# Examples:
#
#   cities = City.create([{ name: 'Chicago' }, { name: 'Copenhagen' }])
#   Mayor.create(name: 'Emanuel', city: cities.first)
Section.create(:parent_id => 0, :title => "- Корень -")
Setting.create(:option => 'delivery_limit', :title => 'Порог бесплатной доставки', :value => 800, :value_type => 0)
Setting.create(:option => 'delivery_value', :title => 'Стоимость доставки', :value => 200, :value_type => 0)
Setting.create(:option => 'items_per_page', :title => 'Товаров на странице', :value => 5, :value_type => 0)

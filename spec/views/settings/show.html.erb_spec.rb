require 'spec_helper'

describe "settings/show.html.erb" do
  before(:each) do
    @setting = assign(:setting, stub_model(Setting,
                                           :option => "Option",
                                           :value => "Value"
    ))
  end

  it "renders attributes in <p>" do
    render
    # Run the generator again with the --webrat flag if you want to use webrat matchers
    rendered.should match(/Option/)
    # Run the generator again with the --webrat flag if you want to use webrat matchers
    rendered.should match(/Value/)
  end
end
